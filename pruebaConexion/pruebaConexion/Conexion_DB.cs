﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;



namespace pruebaConexion
{
    public class Conexion_DB
    {

        //SqlConnection con = new SqlConnection("data source = ServidorSQL; initial catalog = BaseDatos; user id = Usuario; password = Contraseña");
        private SqlConnection con;
        private String strConexion = "Data Source = sql5003.site4now.net; Initial Catalog = DB_A48B97_prograWeb; User = DB_A48B97_prograWeb_admin; Password = tecnologico123;";
        private String strPass = "";
        private String nombreDB = "";
        private String server = "";
        private SqlDataAdapter dta;
        private DataSet data;

        public Conexion_DB(){
            strConexion = "";//Cadena de conexión hacia la base de datos
        }
    
        public String ejecutarInsercion(String sql){
            String res = "Error en la inserción del registro";
            try
            {
                //Iniciamos la conexión, hacemos la ejecución de la consulta sql y la cerramos
               // con = new SqlConnection(strConexion);
                con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myDB"].ConnectionString);
                data = new DataSet();
               // con.ConnectionString = strConexion;
                con.Open();
                System.Console.Out.Write("conexion abierta");
                res = "Abierta";
                SqlCommand cmd = new SqlCommand(sql, con);

                if (cmd.ExecuteNonQuery() > 0) {
                    res = "Registro almacenado correctamente";
                }
                con.Close();
            }
            catch (SqlException ex) {
                System.Console.Out.Write(ex.Message);
                System.Console.Out.Write("Error en la conexion");
                res = ex.Message;
            }
            return res;
        }


        public DataTable ejecutarSeleccion(String sql)
        {
            String res = "Error en la recuperción de registros";
            DataTable dt = new DataTable();
            try
            {
                //Iniciamos la conexión, hacemos la ejecución de la consulta sql y la cerramos
                // con = new SqlConnection(strConexion);
                con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myDB"].ConnectionString);
                data = new DataSet();
                // con.ConnectionString = strConexion;
                con.Open();
                System.Console.Out.Write("conexion abierta");
                res = "Abierta";
                SqlCommand cmd = new SqlCommand(sql, con);                
                SqlDataReader dr = cmd.ExecuteReader();                
                dt.Load(dr);                
                con.Close();
            }
            catch (SqlException ex)
            {
                System.Console.Out.Write(ex.Message);
                System.Console.Out.Write("Error en la conexion");
                res = ex.Message;
            }
            return dt;
        }
  }
}