﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pruebaConexion
{
    public partial class inicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (!txtNombre.Text.Trim().Equals("") && !txtApellido.Text.Trim().Equals(""))
            {
                Conexion_DB conexion = new Conexion_DB();
                String sql = "INSERT INTO usuarios(nombre, apellidoP) VALUES ('" + txtNombre.Text.Trim() + "','" + txtApellido.Text.Trim() + "')";
                string res = conexion.ejecutarInsercion(sql);

                // String resultado = conexion.ejecutarSeleccion();
                string script = "alert(\"" + res + "!\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                                      "ServerControlScript", script, true);

                txtApellido.Text = "";
                txtNombre.Text = "";

            }
            else {
                string script = "alert(No hay valores a insertar!);";
                ScriptManager.RegisterStartupScript(this, GetType(),
                                      "ServerControlScript", script, true);
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Conexion_DB conexion = new Conexion_DB();
            String sql = "SELECT * FROM usuarios";
            grvDatos.DataSource = conexion.ejecutarSeleccion(sql);
            grvDatos.DataBind();

            
        }
    }
}