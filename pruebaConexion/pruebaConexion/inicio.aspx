﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inicio.aspx.cs" Inherits="pruebaConexion.inicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #form1 {
            height: 298px;
        }
        .auto-style1 {
            width: 143px;
        }
        .auto-style2 {
            width: 234px;
        }
    </style>
</head>
<body style="height: 397px">
    <form id="form1" runat="server">
        <table style="width:100%;" title="Insertar">
            <tr>
                <td class="auto-style1">Insertar</td>
                <td class="auto-style2">Recuperar</td>
                <td>Borrar</td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label1" runat="server" Text="Nombre: "></asp:Label>
                    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="Apellido: "></asp:Label>
                    <asp:TextBox ID="txtApellido" runat="server"></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Insertar" />
                </td>
                <td class="auto-style2">
                    <asp:GridView ID="grvDatos" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                    </asp:GridView>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
